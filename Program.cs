﻿using Store.Products;
using System;
using System.Collections.Generic;

namespace Store
{
    class Program
    {
        static void Main(string[] args)
        {
            Appliance appliance = new Appliance();
            appliance.Name = "Phone";
            appliance.Brand = "Samsung";
            appliance.Price = 2345;
            appliance.Model = "A12";
            appliance.ProductionDate = new DateTime(2021, 03, 03);
            appliance.Weight = 0.6;

            Beverage beverage = new Beverage();
            beverage.Name = "Ice tea";
            beverage.Brand = "Nestea";
            beverage.Price = 0.99M;
            beverage.ExpirationDate = new DateTime(2021, 06, 24);

            Food food = new Food();
            food.Name = "Bananas";
            food.Brand = "BananaLand";
            food.Price = 1.50M;
            food.ExpirationDate = new DateTime(2021, 06, 20);

            Clothes clothes = new Clothes();
            clothes.Name = "Dress";
            clothes.Brand = "Zara";
            clothes.Price = 15.99M;
            clothes.Size = "S";
            clothes.Color = "blue";

            List<CartProduct> cart = new List<CartProduct>();
            cart.Add(new CartProduct {Product = appliance, Quantity = 1});
            cart.Add(new CartProduct {Product = beverage, Quantity = 3});
            cart.Add(new CartProduct {Product = food, Quantity = 2.45});
            cart.Add(new CartProduct {Product = clothes, Quantity = 2});

            Cashier.Checkout(cart, new DateTime(2021, 06, 20));
        }
    }
}
