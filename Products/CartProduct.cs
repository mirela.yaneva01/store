﻿namespace Store.Products
{
    class CartProduct
    {
        public Product Product { get; set; }

        public double Quantity { get; set; }
    }
}
