﻿using System;

namespace Store.Products
{
    abstract class Product
    {
        public string Name { get; set; }

        public string Brand { get; set; }

        public decimal Price { get; set; }

        public abstract int GetDiscount(DateTime dateOfPurchase);
    }
}
