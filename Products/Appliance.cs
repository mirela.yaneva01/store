﻿using System;

namespace Store.Products
{
    class Appliance : Product
    {
        public string Model { get; set; }

        public DateTime ProductionDate { get; set; }

        public double Weight { get; set; }

        public override int GetDiscount(DateTime dateOfPurchase)
        {
            if ((dateOfPurchase.DayOfWeek == DayOfWeek.Sunday || dateOfPurchase.DayOfWeek == DayOfWeek.Saturday) && Price > 999)
            {
                return 5;
            }
            return 0;
        }
    }
}
