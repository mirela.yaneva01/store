﻿using System;

namespace Store.Products
{
    abstract class Perishable : Product
    {
        public DateTime ExpirationDate { get; set; }

        public override int GetDiscount(DateTime dateOfPurchase)
        {
            if (ExpirationDate.AddDays(-5) < dateOfPurchase && ExpirationDate > dateOfPurchase)
            {
                return 10;
            }
            else if (ExpirationDate == dateOfPurchase)
            {
                return 50;
            }
            return 0;
        }
    }
}
