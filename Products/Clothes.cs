﻿using System;

namespace Store.Products
{
    class Clothes : Product
    {
        public string Size { get; set; }

        public string Color { get; set; }

        public override int GetDiscount(DateTime dateOfPurchase)
        {
            if (dateOfPurchase.DayOfWeek != DayOfWeek.Sunday && dateOfPurchase.DayOfWeek != DayOfWeek.Saturday)
            {
                return 10;
            }
            return 0;
        }
    }
}
