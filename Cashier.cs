﻿using Store.Products;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace Store
{
    class Cashier
    {
        public static void Checkout(List<CartProduct> cart, DateTime dateOfPurchase)
        {
            decimal subTotal = 0;
            decimal discount = 0;
            CultureInfo.CurrentCulture = CultureInfo.CreateSpecificCulture("en-US");

            Console.WriteLine($"Date: {dateOfPurchase}");
            Console.WriteLine("---Products---");

            foreach (var cartProduct in cart)
            {
                int discountPercent = cartProduct.Product.GetDiscount(dateOfPurchase);
                decimal productTotal = (decimal)cartProduct.Quantity * cartProduct.Product.Price;
                subTotal = subTotal + productTotal;

                Console.WriteLine();
                Console.WriteLine();
                if (cartProduct.Product.GetType() == typeof(Clothes))
                {
                    Clothes clothesProduct = (Clothes)cartProduct.Product;
                    Console.WriteLine($"{clothesProduct.Name} {clothesProduct.Brand} {clothesProduct.Size} {clothesProduct.Color}");
                }
                else if(cartProduct.Product.GetType() == typeof(Appliance))
                {
                    Appliance applianceProduct = (Appliance)cartProduct.Product;
                    Console.WriteLine($"{applianceProduct.Name} {applianceProduct.Brand} {applianceProduct.Model}");
                }
                else
                {
                    Console.WriteLine($"{cartProduct.Product.Name} {cartProduct.Product.Brand}");
                }
                Console.WriteLine($"{cartProduct.Quantity} X {cartProduct.Product.Price:C} = {productTotal:C}");
                if (discountPercent > 0)
                {
                    decimal productDiscount = productTotal * discountPercent / 100;
                    discount = discount + productDiscount;

                    Console.WriteLine($"#discount {discountPercent}% -{productDiscount:C}");
                }
            }

            Console.WriteLine("-----------------------------------------------------------------------------------");
            Console.WriteLine();
            Console.WriteLine($"SUBTOTAL: {subTotal:C}");
            Console.WriteLine($"DISCOUNT: -{discount:C}");
            Console.WriteLine();
            Console.WriteLine($"TOTAL: {subTotal - discount:C}");
        }
    }
}
